from django.shortcuts import render
from django.http import JsonResponse
import os
import uuid
import datetime
# Create your views here.

def _main_view_(request):
    return render(request,'activities/main_activity.html',{})

def _get_dom_message_(request,player_name):
    message = request.POST['message']
    return render(request,'dom_data/message_dom.html', {'player_name':player_name,'message':message})

def _get_dom_message_rcv(request,player_name):
    message = request.POST['message']
    return render(request,'dom_data/message_dom_rcv.html',{'player_name':player_name,'message':message})

def _get_server_data_(request):
    SERVER_TYPE = str(os.environ.get('SERVER_TYPE'))
    return JsonResponse({'SERVER_TYPE' : SERVER_TYPE,'SERVER_TIME':datetime.datetime.now()})
