
function start_render(){
  $.getScript("https://cdn.jsdelivr.net/npm/phaser@3.15.1/dist/phaser-arcade-physics.min.js").then(function(){
    var config = {
      type:Phaser.AUTO,
      width:800,
      height:600,
      parent:'render_game',
      physics: {
        default:'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
      },
      scene: {preload:preload,create:create,update:update}
    };

    var game = new Phaser.Game(config);

  },null);

var platforms;
function preload(){
  this.load.image('sky', '../static/sky.png');
  this.load.image('ground', '../static/platform.png');
  this.load.image('star', '../static/star.png');
  this.load.image('bomb', 'assets/bomb.png');
  this.load.spritesheet('dude',
      '../static/dude.png',
      { frameWidth: 32, frameHeight: 48 }
  );
}
var cursors;
function create(){
  this.add.image(400,300,'sky');
  this.add.image(400,300,'star');
  platforms = this.physics.add.staticGroup();
  platforms.create(400,568,'ground').setScale(2).refreshBody();
  platforms.create(600, 400, 'ground');
 platforms.create(50, 250, 'ground');
 platforms.create(750, 220, 'ground');

 player = this.physics.add.sprite(0, 450, 'dude');

player.setBounce(0.2);
player.setCollideWorldBounds(true);

this.anims.create({
    key: 'left',
    frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
    frameRate: 10,
    repeat: -1
});

this.anims.create({
    key: 'turn',
    frames: [ { key: 'dude', frame: 4 } ],
    frameRate: 20
});

this.anims.create({
    key: 'right',
    frames: this.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
    frameRate: 10,
    repeat: -1
});

this.physics.add.collider(player, platforms);
cursors = this.input.keyboard.createCursorKeys();
var start;
stars = this.physics.add.group({
    key: 'star',
    repeat: 11,
    setXY: { x: 12, y: 0, stepX: 70 }
});

stars.children.iterate(function (child) {

    child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));

});
}
function update(){if (cursors.left.isDown)
{
    player.setVelocityX(-160);

    player.anims.play('left', true);
}
else if (cursors.right.isDown)
{
    player.setVelocityX(160);

    player.anims.play('right', true);
}
else
{
    player.setVelocityX(0);

    player.anims.play('turn');
}

if (cursors.up.isDown && player.body.touching.down)
{
    player.setVelocityY(-330);
}
}

}
