var start_point_http;
var start_point_ws;
var game_player_name;
var player_node;
$(document).ready(function() {
  $('body').bootstrapMaterialDesign();
  get_server_data().then(function(e){
    init(e['SERVER_TYPE']);
    $.getScript(window.start_point_http+window.location.host+"/static/js/socket_handler.js", function() {
    }).then(function(){
      window.load_player_script(e);
    },function(error){
      console.log(error);
    });
  },null);
  });

  function get_server_data(){
  return $.ajax({
      url:'/get/server/data/',
      type:'GET',
      dataType:'json'
    });
  }

  function init(data){
    if( data == 'local'){
      window.start_point_http = 'http://';
      window.start_point_ws = 'ws://';
    }
   else{
     window.start_point_http = 'https://';
     window.start_point_ws = 'wss://';
   }
  }

function load_player_script(e){
  $.getScript(window.start_point_http+window.location.host+"/static/js/class_player.js", function() {
  }).then(function(){
    window.player_node = new Player('Anonymous_'.concat(makeid(5)),e['SERVER_TIME'],[],0,0,'Room');
    window.start_socket(window.player_node.room);
    init_message();
    init_engine();
  },function(error){
    console.log(error);
  });
}

function init_engine(){
  $.getScript(window.start_point_http+window.location.host+"/static/js/game_renderer.js",function(){
   window.start_render();
  }).then(function(){},null);

}
function init_message(){
  $('#_form_messaging_').on('submit', function(event){
    event.preventDefault();
    var layout =  $('#_form_messaging_value_');
    var _text_value_ = $('#_form_messaging_value_').val();
    var _dom_holder_ = $('#_message_holder_');
    window.send_message(_text_value_,layout,_dom_holder_);
  });
}

function makeid(length) {
 var result           = '';
 var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
 var charactersLength = characters.length;
 for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));

 }
 return result;
}
