
var main_socket_;
function start_socket(room_name_){
  if(window.main_socket_ == null){
  window.main_socket_ = new WebSocket(window.start_point_ws + window.location.host + '/ws/join/' + room_name_ + '/');
  window.main_socket_.onmessage = function(e){
    var data = JSON.parse(e.data);
    var message = data['message'];
    var sender = data['sender'];
    var layout =  $('#_form_messaging_value_');
    var _dom_holder_ = $('#_message_holder_');
    if(sender != window.player_node.name){
      recieving(message,layout,_dom_holder_,sender)
    }
  }
  window.main_socket_.onclose = function(e){
    console.log("CLOSE");
    window.main_socket_=null;
  }
  window.main_socket_.onopen = function(e){
    console.log("OPENED");
  }
    }
  else{
    window.main_socket_.close();
    window.main_socket_ = null;
  }
  return window.main_socket_;
}


function send_message(text,layout,holder){
  if(window.main_socket_!= null){
    window.main_socket_.send(JSON.stringify({'message':text,'sender':window.player_node.name}),callback_send(text,layout,holder));
    return true;
  }
  else{
    return false;
  }
}

function callback_send(text,layout,holder){
  layout.val('');
  get_Data_message_(text,window.player_node.name).then(function(e){
  window.player_node.messages.push(text);
  holder.append(e);
  holder.children().show('slow');
  },on_failure_callback());
}

function recieving(text,layout,holder,sender){
  layout.val('');
  get_Data_message_rcv(text,sender).then(function(e){
  window.player_node.messages.push(text);
  holder.append(e);
  holder.children().show('slow');
  },on_failure_callback());
}

function get_Data_message_(text,player_name){
  return $.ajax({
    url:'/get/message/'+player_name.toString()+'/',
    type:'POST',
    dataType:'html',
    data: {  'csrfmiddlewaretoken':$('input[name="csrfmiddlewaretoken"]').val(),
    'message':text,
  'player_name':player_name,}
  });
}

function get_Data_message_rcv(text,player_name){
  return  $.ajax({
    url:'/get/message/'+player_name.toString()+'/rcv/',
    type:'POST',
    dataType:'html',
    data: {  'csrfmiddlewaretoken':$('input[name="csrfmiddlewaretoken"]').val(),
    'message':text,
  'player_name':player_name,}
  });
}

function on_done_callback(data){
}

function on_failure_callback(reason){
  return reason;
}
