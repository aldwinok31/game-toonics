from django.apps import AppConfig


class ChatguessConfig(AppConfig):
    name = 'chatguess'
