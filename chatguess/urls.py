from django.urls import path
from . import views
app_name="chatguess"
urlpatterns = [
  path('',views._main_view_,name="base_view"),
  path('get/message/<str:player_name>/',views._get_dom_message_,name="messaging_dom_"),
  path('get/server/data/',views._get_server_data_,name="server_time"),
  path('get/message/<str:player_name>/rcv/',views._get_dom_message_rcv,name="messaging_rcv"),
]
